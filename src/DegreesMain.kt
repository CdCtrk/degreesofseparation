import kotlin.collections.ArrayList
import kotlin.collections.HashMap

// Degrees of Separation
//
// In this coding exercise, we will calculate the minimal degrees of
// separation between two actors. For example, according to the data set
// supplied below, Pierce Brosnan and Tilda Swinton are 2 degrees apart,
// because Brosnan was in a movie with Meryl Streep, and Streep was in
// a different movie with Tilda Swinton.
//
// For more information on the inspiration for this exercise, see
// https://en.wikipedia.org/wiki/Six_Degrees_of_Kevin_Bacon
//
// To complete this exercise,
// try to get as many of the Test Cases as you can to pass.
//
// Please do not spend more than a couple of hours on this.
// It is meant to replace the in-person coding that many companies
// use for interviews, so you can work in peace and at your own pace.
// It is not meant to take up a ton of your time.
//
// We are looking for
//   how you approach the problem,
//   how you structure and organize your code,
//   how readable your code is to someone who is not familiar with it,
//   and lastly, whether your code produces accurate results.
//
// Make whatever additional classes, if any, you want to make.
// Feel free to modify the existing MovieData class if you want.
// Feel free to add more test cases if it helps you during development.
//
// If anything about this exercise is unclear,
//   make whatever assumptions you need to unblock yourself,
//   and leave a comment explaining what you assumed and how it helped you.
//
// Thanks, good luck, and have fun!

var actorList = ArrayList<String>()
var movieList = ArrayList<String>()
var graph = ArrayList<ActorNode>()

fun main(args: Array<String>) {
    // Get the movie cast data
    val movieData: String
    if (args.isNullOrEmpty()) movieData = SampleMovieData.getMovieData()
    else movieData = args[0]

    // Parse the Movie Data
    val movieMap = parseMovieData(movieData)

    // Ask user for actors
    buildGraph(movieMap)

//    var startingActor = "Robin Williams"
//    var finishActor = "Nicolas Cage"
//    Mamma Mia
//    var startingActor = "Pierce Brosnan"
//    var finishActor = "Meryl Streep"
//    2
//    var startingActor = "Pierce Brosnan"
//    var finishActor = "Tilda Swinton"
    var startingActor = "Robin Williams"
    var finishActor = "Nicolas Cage"
//    var startingActor = "Robin Williams"
//    var finishActor = "Nicolas Cage"
//    var startingActor = "Robin Williams"
//    var finishActor = "Nicolas Cage"
    var foundConnection = findConnection(startingActor, finishActor)
    var startingActorNode = getActorNodeForActor(startingActor)
    var finishActorNode = getActorNodeForActor(finishActor)
    var output = ""
    var degrees = 0
    if (foundConnection != -1) {
        println("Found Connection.")
        var lastNode = graph[foundConnection]
        var originNode = graph[lastNode.originatingNodeIndex]
        var movieLink = originNode.relatedActors.find { it.actorIndex == foundConnection }
        output += lastNode.actorName + " was in " + movieList[movieLink!!.movieIndex] + " with " + originNode.actorName
        degrees++
        while(originNode.originatingNodeIndex != -1 && originNode.nodeIndex != finishActorNode!!.nodeIndex) {
            lastNode = originNode
            originNode = graph[lastNode.originatingNodeIndex]
            movieLink = originNode.relatedActors.find { it.actorIndex == lastNode.nodeIndex }
            output += " who was in " + movieList[movieLink!!.movieIndex] + " with " + originNode.actorName
            degrees++
        }
        output += ". \n The degrees of separation between $startingActor and $finishActor is $degrees."
    }
    println(output)

}

fun parseMovieData(movieData: String): Map<String, List<String>> {
    if (movieData.isBlank()) throw Exception("MovieData is blank, please use proper input")

    val result = HashMap<String, ArrayList<String>>()
    var newMovie = true
    var currentMovie = ""
    var currentActors = ArrayList<String>()

    movieData.lines().forEach {
        when {
            it.isBlank() -> {
                newMovie = true;
                result[currentMovie] = currentActors
            }
            newMovie -> {
                newMovie = false
                currentMovie = it
                currentActors = ArrayList()
            }
            else -> currentActors.add(it)
        }
    }
    return result
}

fun findConnection(startingActor: String, finishActor: String): Int {
    var queue = Queue<ActorNode>()

    //Handle if actor1 or actor2 does not exist in graph
    var startingActorIndex:Int? = getActorNodeIndex(startingActor) ?: return -1;
    var finishActorIndex: Int? = getActorNodeIndex(finishActor) ?: return -1;

    // Start the queue at the target
    queue.enqueue(graph[finishActorIndex!!])

    while (!queue.isEmpty()) {
        var actorNode = queue.dequeue()

        actorNode!!.relatedActors.forEach {
            var linkedActorNode = graph[it.actorIndex]
            if (!linkedActorNode.visited) {
                linkedActorNode.visited = true
                queue.enqueue(linkedActorNode)
                linkedActorNode.originatingNodeIndex = actorNode.nodeIndex
                // If this is the actor we are searching for, return the node index
                    if(linkedActorNode.nodeIndex == startingActorIndex) return linkedActorNode.nodeIndex
            }
        }
    }
    return -1
}

// Add first actor to graph
// Add next actor to graph, connect to actor1
// Add to graph actor1 -> actor2, movie

fun buildGraph(movieMap: Map<String, List<String>>) {
    var currentMovieIndex = 0
    movieMap.forEach {
        buildTreeForMovie(it.key, it.value)
    }
//    println(movieList)
//    graph.forEach { println(it) }
}

fun buildTreeForMovie(movie: String, actorsInMovie: List<String>) {
    if (!movieList.contains(movie)) {
        var currentMovieIndex = movieList.size
        movieList.add(movie)

        var linkedActorNodes = ArrayList<ActorNode>()
        actorsInMovie.forEach { actor ->
            var actorNode = getActorNodeForActor(actor)
            if (actorNode == null) {
                var node = ActorNode(actor, graph.size, false, -1, ArrayList())
                linkedActorNodes.add(node)
                graph.add(node)
            } else {
                linkedActorNodes.add(actorNode)
            }

        }

        // Link all actors in current movie together
        actorsInMovie.forEach { actor1 ->
            linkedActorNodes.forEach { linkedActorNode ->
                addLink(actor1, linkedActorNode.nodeIndex, currentMovieIndex)
            }
        }
    }
}

fun addLink(actorName: String, linkedActorIndex: Int, movieIndex: Int) {
    var actorNode = graph.find {
        it.actorName == actorName
    }
    actorNode!!.relatedActors.add(MovieConnection(linkedActorIndex, movieIndex))
}

fun getActorNodeForActor(actorName: String): ActorNode? {
    var actorNodeIndex = getActorNodeIndex(actorName)
    return if(actorNodeIndex != null) graph[getActorNodeIndex(actorName)!!] else null
}

fun getActorNodeIndex(actorName: String): Int? {
    return graph.find { it.actorName == actorName }?.nodeIndex
}
class SampleMovieData {
    companion object {
        public fun getMovieData():String {
            return "GoldenEye\n" +
                    "Pierce Brosnan\n" +
                    "Sean Bean\n" +
                    "Izabella Scorupco\n" +
                    "Judi Dench\n" +
                    "Desmond Llewelyn\n" +
                    "Minnie Driver\n" +
                    "\n" +
                    "Mama Mia!\n" +
                    "Meryl Streep\n" +
                    "Amanda Seyfried\n" +
                    "Pierce Brosnan\n" +
                    "Colin Firth\n" +
                    "Stellan Skarsgard\n" +
                    "\n" +
                    "Good Will Hunting\n" +
                    "Matt Damon\n" +
                    "Robin Williams\n" +
                    "Ben Affleck\n" +
                    "Stellan Skarsgard\n" +
                    "Minnie Driver\n" +
                    "\n" +
                    "Ronin\n" +
                    "Robert De Niro\n" +
                    "Jean Reno\n" +
                    "Natascha McElhone\n" +
                    "Sean Bean\n" +
                    "Stellan Skarsgard\n" +
                    "Jonathan Pryce\n" +
                    "\n" +
                    "Awakenings\n" +
                    "Robin Williams\n" +
                    "Robert De Niro\n" +
                    "Julie Kavner\n" +
                    "John Heard\n" +
                    "Penelope Ann Miller\n" +
                    "Max von Sydow\n" +
                    "\n" +
                    "Adaptation\n" +
                    "Nicolas Cage\n" +
                    "Meryl Streep\n" +
                    "Chris Cooper\n" +
                    "Cara Seymour\n" +
                    "Tilda Swinton\n" +
                    "Ron Livingston\n" +
                    "Maggie Gyllenhaal\n" +
                    "Judy Greer\n" +
                    "\n" +
                    "National Treasure\n" +
                    "Nicolas Cage\n" +
                    "Sean Bean\n" +
                    "Harvey Keitel\n" +
                    "Jon Voight\n" +
                    "Justin Bartha\n" +
                    "Diane Kruger\n" +
                    "Christopher Plummer\n" +
                    "\n" +
                    "Mission: Impossible\n" +
                    "Tom Cruise\n" +
                    "Jon Voight\n" +
                    "Emmanuelle Beart\n" +
                    "Ving Rhames\n" +
                    "Vanessa Redgrave\n" +
                    "Henry Czerny\n" +
                    "Jean Reno\n" +
                    "Emilio Estevez";
        }
    }

}
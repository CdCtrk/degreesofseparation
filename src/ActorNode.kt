data class ActorNode(var actorName: String, val nodeIndex: Int, var visited: Boolean, var originatingNodeIndex: Int, var relatedActors: ArrayList<MovieConnection>) {
    override fun toString(): String {
        var relatedActorsString = ""
        relatedActors.forEach { relatedActorsString += "\n    $it"}
        return "ActorNode($actorName, $nodeIndex, $relatedActorsString)"
    }
}

